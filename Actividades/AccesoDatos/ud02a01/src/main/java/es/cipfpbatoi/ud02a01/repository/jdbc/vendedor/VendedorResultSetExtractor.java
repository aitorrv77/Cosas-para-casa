package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import es.cipfpbatoi.ud02a01.model.Vendedores;

public class VendedorResultSetExtractor implements ResultSetExtractor<Vendedores>{

	@Override
	public Vendedores extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(!rs.next()) {
		return null;
		}
		
		Vendedores vendedor = new Vendedores();
		
		vendedor.setId(rs.getInt("id"));
		vendedor.setNombre(rs.getString("nombre"));
		vendedor.setSalario(rs.getDouble("salario"));
		vendedor.setFechaIngreso(rs.getDate("fecha_ingreso"));
		
		return vendedor;
	}

}

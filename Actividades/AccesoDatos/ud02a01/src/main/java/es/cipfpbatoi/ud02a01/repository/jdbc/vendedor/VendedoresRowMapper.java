package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Vendedores;

public class VendedoresRowMapper implements RowMapper<Vendedores>{

	@Override
	public Vendedores mapRow(ResultSet rs, int rowNum) throws SQLException {
		Vendedores vendedor = new Vendedores();
		vendedor.setId(rs.getInt("id"));
		vendedor.setNombre(rs.getString("nombre"));
		vendedor.setFechaIngreso(rs.getDate("fecha_ingreso"));
		vendedor.setSalario(rs.getDouble("salario"));
		return vendedor;
	}
}

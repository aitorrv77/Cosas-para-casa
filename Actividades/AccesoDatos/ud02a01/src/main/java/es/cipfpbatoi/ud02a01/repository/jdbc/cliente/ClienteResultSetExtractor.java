package es.cipfpbatoi.ud02a01.repository.jdbc.cliente;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import es.cipfpbatoi.ud02a01.model.Cliente;

public class ClienteResultSetExtractor implements ResultSetExtractor<Cliente> {

	@Override
	public Cliente extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		if(!rs.next()) {
			return null;
		}
		
		Cliente cliente = new Cliente();
		
		cliente.setId(rs.getInt("id"));
		cliente.setNombre(rs.getString("nombre"));
		cliente.setDireccion(rs.getString("direccion"));
		
		return cliente;
	}
}


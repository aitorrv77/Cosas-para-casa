package es.cipfpbatoi.ud02a01.repository.jdbc.lineasFacturas;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import es.cipfpbatoi.ud02a01.model.LineasFactura;

public class LineasFacturaResultSetExtractor implements ResultSetExtractor<LineasFactura>{

	@Override
	public LineasFactura extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(!rs.next()) {
			return null;
		}
		
		LineasFactura lineaFactura = new LineasFactura();
		
		lineaFactura.setLinea(rs.getInt("linea"));
		lineaFactura.setIdFactura(rs.getInt("factura"));
		lineaFactura.setIdArticulo(rs.getInt("articulo"));
		lineaFactura.setCantidad(rs.getInt("cantidad"));
		lineaFactura.setImporte(rs.getFloat("importe"));
		return lineaFactura;
	}

}
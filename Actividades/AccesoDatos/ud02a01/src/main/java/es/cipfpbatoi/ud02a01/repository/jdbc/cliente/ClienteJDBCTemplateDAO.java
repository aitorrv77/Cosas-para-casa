package es.cipfpbatoi.ud02a01.repository.jdbc.cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;

@Repository
public class ClienteJDBCTemplateDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT =  "SELECT COUNT(*) FROM CLIENTES";
	
	private static final String SELECT_ALL =  "SELECT * FROM CLIENTES";
	private static final String SELECT_BY_ID =  "SELECT * FROM CLIENTES WHERE ID = ?";
	
	private static final String INSERT =  "INSERT INTO clientes(nombre, direccion) VALUES(?,?)";
	private static final String UPDATE =  "UPDATE clientes SET nombre=?, direccion=? WHERE id=?";
	private static final String DELETE =  "DELETE clientes WHERE id=?";
	
	
	
	public int count() {
		return jdbcTemplate.queryForObject(ClienteJDBCTemplateDAO.SELECT_COUNT, Integer.class);
	}
	
	public List<Cliente> findAll() {
		
		List<Cliente> clientes = this.jdbcTemplate.query(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
						return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
	            
					}
			
				}, 
				new ClientesRowMapper()
		);
		
		return clientes;
	}
	
	public Cliente findById(Integer id) {
		
		Cliente cliente = this.jdbcTemplate.query(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
				PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
	            ps.setInt(1, id);
	            return ps;
	            
			}
			
		}, new ClienteResultSetExtractor());
		
		return cliente;
	}
	
	public Cliente insert(Cliente cliente) {
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
						ps.setString(1,cliente.getNombre());
						ps.setString(2, cliente.getDireccion());
						return ps;
					}
					
				}, keyHolder);
		if(resultadoOperacion != 1) {
			return null;
		}
		cliente.setId(keyHolder.getKey().intValue());
		return cliente;
	}
	
	public Boolean update(Cliente cliente) {
		int resultadoOperacion = this.jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
				
				ps.setString(1, cliente.getNombre());
				ps.setString(2, cliente.getDireccion());
				ps.setInt(3, cliente.getId());
				return ps;
			}});
		return resultadoOperacion == 1;
	}
	
	public Boolean delete(Cliente cliente) {
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {

					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(DELETE, Statement.CLOSE_CURRENT_RESULT);
						ps.setInt(1, cliente.getId());
						return ps;
					}
					
				});
		
		
		return resultadoOperacion == 1;
	}
	
}

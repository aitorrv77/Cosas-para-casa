package es.cipfpbatoi.ud02a01.repository.csv;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Factura;
import es.cipfpbatoi.ud02a01.model.LineasFactura;


@Repository
public class FacturasCsvDAO {
	
	public File writeCSV(Factura facturas) {
		Factura factura = new Factura();
		String palabra;
		float totalImporte = 0;
		
		File file;
		
		try {
			file = File.createTempFile("facturas", ".csv");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			OutputStream ou = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(ou, "UTF-8");
			BufferedWriter bf = new BufferedWriter(osw);
			
			bf.write("----------------------------------------------");
			bf.write("-Factura: "+factura.getId());
			bf.write("Fecha: "+factura.getFecha()+". -");
			bf.newLine();
			bf.write("-Articulos");
			bf.write("CANTIDAD");
			bf.write("IMPORTE. -");
			bf.newLine();
			bf.write("----------------------------------------------");
			bf.newLine();
			for(LineasFactura lfactura:factura.getlFactura()) {
				bf.write("Articulo: ");
				bf.write(lfactura.getLinea());
				bf.write(lfactura.getIdArticulo());
				bf.write(lfactura.getCantidad());
				bf.write((int) lfactura.getImporte());
				totalImporte = lfactura.getImporte()+lfactura.getImporte();
			}
			bf.newLine();
			bf.write("----------------------------------------------");
			bf.newLine();
			bf.write("TOTAL");
			bf.write((int) totalImporte);
			bf.newLine();
			bf.newLine();
			bf.write("Cliente: "+ factura.getCliente());
			bf.write("Vendedor: "+factura.getVendedor());
			bf.newLine();
			bf.write("Forma de pago: "+factura.getFormaPago());
			return file;
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	

	
}

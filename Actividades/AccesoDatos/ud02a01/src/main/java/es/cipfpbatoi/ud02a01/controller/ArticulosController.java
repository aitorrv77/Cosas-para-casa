package es.cipfpbatoi.ud02a01.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import es.cipfpbatoi.ud02a01.model.Articulo;
import es.cipfpbatoi.ud02a01.repository.jdbc.articulos.ArticuloJDBCTemplateDAO;

@RestController
@RequestMapping("/articulos")
public class ArticulosController {
	
	@Autowired
	ArticuloJDBCTemplateDAO articuloJDBCTemplateDAO;
	
	@PostMapping("")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Articulo create(@RequestBody Articulo articulo) {
		
		Articulo articuloInsertado = this.articuloJDBCTemplateDAO.insert(articulo);
		if(articuloInsertado == null) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entidad no creada");
		}
		return articuloInsertado;
	}
	
	
	
	@GetMapping("/count")
	public Integer count() {
		return this.articuloJDBCTemplateDAO.count();
	}
	
	@GetMapping("")
	public List<Articulo> findAll(){
		return this.articuloJDBCTemplateDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public Articulo findById(@PathVariable int id) {
		Articulo articulo = this.articuloJDBCTemplateDAO.findById(id);
		if(articulo == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Entidad no encontrada");
		}
		return articulo;
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entidad actualizada")
	public void update(@PathVariable int id, @RequestBody Articulo articulo) {
		
		boolean update = this.articuloJDBCTemplateDAO.update(articulo);
		if(!update) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entidad no actualizada");
		}
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entidad eliminada")
	public void delete(@PathVariable int id, @RequestBody Articulo articulo) {
		
		boolean deleted = this.articuloJDBCTemplateDAO.delete(articulo);
		if(!deleted) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entidad no eliminada");
		}
	}
	
}

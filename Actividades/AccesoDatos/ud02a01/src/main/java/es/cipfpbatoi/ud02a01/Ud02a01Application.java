package es.cipfpbatoi.ud02a01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud02a01Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud02a01Application.class, args);
	}

}

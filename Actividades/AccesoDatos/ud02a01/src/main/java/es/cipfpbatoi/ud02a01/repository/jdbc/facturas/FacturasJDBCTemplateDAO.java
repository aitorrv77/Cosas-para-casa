package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Factura;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCTemplateDAO;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteResultSetExtractor;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClientesRowMapper;

@Repository
public class FacturasJDBCTemplateDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT =  "SELECT COUNT(*) FROM FACTURAS";
	
	private static final String SELECT_ALL =  "SELECT * FROM FACTURAS";
	private static final String SELECT_BY_ID =  "SELECT * FROM FACTURAS WHERE ID = ?";
	
	private static final String SELECT_WITH_LINEAS_CLIENTE_VENDEDOR = 
			"SELECT * FROM facturas as f "
			+ "INNER JOIN lineas_factura as l ON (f.id = l.factura) "
			+ "INNER JOIN clientes as c ON(c.id = f.cliente) "
			+ "INNER JOIN vendedores as v ON(v.id = f.vendedor) WHERE 1=1";
	
	private static final String SELECT_BY_ID_WITH_ALL = 
			"SELECT * FROM facturas as f "
					+ "INNER JOIN lineas_factura as l ON (f.id = l.factura) "
					+ "INNER JOIN clientes as c ON(c.id = f.cliente) "
					+ "INNER JOIN vendedores as v ON(v.id = f.vendedor) WHERE f.id = ?";
	
	private static final String INSERT =  "INSERT INTO facturas(fecha, cliente, vendedor, formaPago) VALUES(?,?,?,?)";
	private static final String UPDATE =  "UPDATE facturas SET fecha=?, cliente=?, vendedor=?, formaPago=? WHERE id=?";
	private static final String DELETE =  "DELETE facturas WHERE id=?";
	
	public int count() {
		return jdbcTemplate.queryForObject(FacturasJDBCTemplateDAO.SELECT_COUNT, Integer.class);
	
	}
	
	public List<Factura> findWithAll() {
		List<Factura> facturas = this.jdbcTemplate.query(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				
				return con.prepareStatement(SELECT_WITH_LINEAS_CLIENTE_VENDEDOR, Statement.CLOSE_CURRENT_RESULT);
			}
		},new FacturasWithAllRowMapper());
		
		// Eliminamos los duplicados con un stream
		facturas = facturas.stream().distinct().collect(Collectors.toList());
		return facturas;
	}
	
	public Factura findByIdWithAll(Integer id) {
		Factura factura = this.jdbcTemplate.query(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(SELECT_BY_ID_WITH_ALL,Statement.CLOSE_CURRENT_RESULT);
				ps.setInt(1, id);
				return ps;
			}
		}, new FacturasWithAllResultSetExtractor());
		
		return factura;
	}
	
	public List<Factura> findAll() {
		
		List<Factura> facturas = this.jdbcTemplate.query(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
						return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
	            
					}
			
				}, 
				new FacturasRowMapper()
		);
		
		return facturas;
	}

	public Factura findById(Integer id) {
	
		Factura factura = this.jdbcTemplate.query(new PreparedStatementCreator() {
		
		@Override
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			
			PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
            ps.setInt(1, id);
            return ps;
            
		}
		
	}, new FacturasResultSetExtractor());
	
	return factura;
}

	public Factura insert(Factura factura) {
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
						ps.setDate(1,factura.getFecha());
						ps.setInt(2, factura.getIdcliente());
						ps.setInt(3, factura.getIdvendedor());
						ps.setString(4, factura.getFormaPago());
						return ps;
					}
					
				}, keyHolder);
		if(resultadoOperacion != 1) {
			return null;
		}
		factura.setId(keyHolder.getKey().intValue());
		return factura;
	}
	
	public Boolean update(Factura factura) {
		int resultadoOperacion = this.jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
				
				ps.setDate(1,factura.getFecha());
				ps.setInt(2, factura.getIdcliente());
				ps.setInt(3, factura.getIdvendedor());
				ps.setString(4, factura.getFormaPago());
				ps.setInt(5, factura.getId());
				return ps;
			}});
		return resultadoOperacion == 1;
	}
	
	public Boolean delete(Factura factura) {
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {

					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(DELETE, Statement.CLOSE_CURRENT_RESULT);
						ps.setInt(1, factura.getId());
						return ps;
					}
					
				});
		
		
		return resultadoOperacion == 1;
	}
	
}

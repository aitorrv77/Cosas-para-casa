package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Grupo;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCTemplateDAO;

@Repository
public class GrupoJDBCTemplateDAO {
 
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT = "SELECT COUNT (*) FROM GRUPOS";
	
	private static final String SELECT_ALL = "SELECT * FROM GRUPOS";
	private static final String SELECT_ALL_WITH_ARTICULOS = "SELECT * FROM grupos as g INNER JOIN articulos as a ON(a.grupo = g.id)";
	
	private static final String SELECT_BY_ID = "SELECT * FROM GRUPOS WHERE ID = ?";
	private static final String SELECT_BY_ID_WITH_ARTICULOS ="";
	
	private static final String INSERT = "INSERT INTO grupos(descripcion) VALUES(?)";
	private static final String UPDATE = "UPDATE grupos SET descripcion =? WHERE id=?";
	private static final String DELETE = "DELETE grupos WHERE id=?";

	public int count() {
	return jdbcTemplate.queryForObject(GrupoJDBCTemplateDAO.SELECT_COUNT,Integer.class);
	}
	
	public List<Grupo> findAll(){
		
		List<Grupo> grupos = this.jdbcTemplate.query(
				new PreparedStatementCreator() {

					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						return con.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
						
					}
					
				},new GrupoRowMapper()
				);
		return grupos;
	}
	
	public Grupo findById(Integer id) {
		Grupo grupo = this.jdbcTemplate.query(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
				ps.setInt(1, id);
				return ps;
			}
			
		},new GrupoResultSetExtractor());
		return grupo;
	}
	
	public Grupo insert(Grupo grupo) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
					
					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(INSERT,Statement.CLOSE_CURRENT_RESULT);
						ps.setString(1, grupo.getDescripcion());
						return ps;
					}
				},keyHolder);
		if(resultadoOperacion != 1) {
			return null;
		}
		grupo.setId(keyHolder.getKey().intValue());
		return grupo;
	}
	
	public Boolean update(Grupo grupo) {
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(UPDATE,Statement.CLOSE_CURRENT_RESULT);
				ps.setString(1, grupo.getDescripcion());
				ps.setInt(2, grupo.getId());
				return ps;
			}
		});
		return resultadoOperacion ==1;
	}
	
	public Boolean delete(Grupo grupo) {
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(DELETE,Statement.CLOSE_CURRENT_RESULT);
				ps.setInt(1, grupo.getId());
				return ps;
			}
		});
		return resultadoOperacion ==1;
	}
}

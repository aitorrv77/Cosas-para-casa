package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Factura;
import es.cipfpbatoi.ud02a01.model.LineasFactura;
import es.cipfpbatoi.ud02a01.model.Vendedores;

public class FacturasWithAllResultSetExtractor implements ResultSetExtractor<Factura>{

	@Override
	public Factura extractData(ResultSet rs) throws SQLException {
		if(!rs.next()) {
			return null;
		}
		
		Factura factura = new Factura();
		factura.setId(rs.getInt("facturas.id"));
		factura.setFecha(rs.getDate("facturas.fecha"));
		factura.setFormaPago(rs.getString("facturas.formapago"));
		
		Cliente cliente = new Cliente();
		factura.setCliente(cliente);
		
		cliente.setId(rs.getInt("clientes.id"));
		cliente.setNombre(rs.getString("clientes.nombre"));
		cliente.setDireccion(rs.getString("clientes.direccion"));
		
		Vendedores vendedor = new Vendedores();
		factura.setVendedor(vendedor);
			
		vendedor.setNombre(rs.getString("vendedores.nombre"));
				
		do {
		LineasFactura lineaFactura = new LineasFactura();
		lineaFactura.setLinea(rs.getInt("lineas_factura.linea"));
		lineaFactura.setIdFactura(rs.getInt("lineas_factura.factura"));
		lineaFactura.setIdArticulo(rs.getInt("lineas_factura.articulo"));
		lineaFactura.setCantidad(rs.getInt("lineas_factura.cantidad"));
		lineaFactura.setImporte(rs.getFloat("lineas_factura.importe"));
		
		
		factura.addLineas(lineaFactura);
		
		}while(rs.next());
		
		return factura;
	}
	

}

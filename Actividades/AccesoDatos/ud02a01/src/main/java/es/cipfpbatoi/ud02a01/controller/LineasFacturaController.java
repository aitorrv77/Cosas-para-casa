package es.cipfpbatoi.ud02a01.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.LineasFactura;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCTemplateDAO;
import es.cipfpbatoi.ud02a01.repository.jdbc.lineasFacturas.LineasFacturaJDBCTemplateDAO;

@RestController
@RequestMapping("/lineas_factura")
public class LineasFacturaController {
	@Autowired
	LineasFacturaJDBCTemplateDAO lineadFacturaJDBCDAOTemplateDAO;
	
	@PostMapping("")
	@ResponseStatus(code=HttpStatus.CREATED)
	public LineasFactura create(@RequestBody LineasFactura lineasFactura) {
		
		LineasFactura lineaFacturaInsertada = this.lineadFacturaJDBCDAOTemplateDAO.insert(lineasFactura);
		if(lineaFacturaInsertada == null) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entitidad no creada");
		}
		
		return lineaFacturaInsertada;
	}
	

	@GetMapping("/count")
	public Integer count() {
		return this.lineadFacturaJDBCDAOTemplateDAO.count();
	}
	
	@GetMapping("")
	public List<LineasFactura> findAll() {
		return this.lineadFacturaJDBCDAOTemplateDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public LineasFactura findById(@PathVariable int id) {
		LineasFactura lineasFactura = this.lineadFacturaJDBCDAOTemplateDAO.findById(id);
		if(lineasFactura == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Entidad no encontrada");
		}
		return lineasFactura;
	}
	
	
	@PutMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason = "Entidad actualizada")
	public void update(@PathVariable int id, @RequestBody LineasFactura lineasFactura) {
		
		boolean update = this.lineadFacturaJDBCDAOTemplateDAO.update(lineasFactura);
		if(!update) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Entidad no actualizada");
		}
		
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entidad eliminada")
	
	public void delete(@PathVariable int id, @RequestBody LineasFactura lineasFactura) {
		
		boolean deleted = this.lineadFacturaJDBCDAOTemplateDAO.delete(lineasFactura);
		if(!deleted) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Entidad no eliminada");
		}
	}
}

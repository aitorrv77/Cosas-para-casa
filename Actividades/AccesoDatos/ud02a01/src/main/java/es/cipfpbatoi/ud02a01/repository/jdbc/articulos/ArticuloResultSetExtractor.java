package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import es.cipfpbatoi.ud02a01.model.Articulo;

public class ArticuloResultSetExtractor implements ResultSetExtractor<Articulo>{

	@Override
	public Articulo extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(!rs.next()) {
		return null;
		}
		
		Articulo articulo = new Articulo();
		articulo.setId(rs.getInt("id"));
		articulo.setNombre(rs.getString("nombre"));
		articulo.setPrecio(rs.getDouble("precio"));
		articulo.setCodigo(rs.getString("codigo"));
		articulo.setGrupo(rs.getInt("grupo"));
		
		return articulo;
		

	}
}
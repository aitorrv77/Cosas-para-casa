package es.cipfpbatoi.ud02a01.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController{

	@GetMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
		
	}
}

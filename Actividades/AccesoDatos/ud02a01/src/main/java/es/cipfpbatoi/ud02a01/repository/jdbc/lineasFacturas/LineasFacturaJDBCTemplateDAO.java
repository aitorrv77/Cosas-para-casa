package es.cipfpbatoi.ud02a01.repository.jdbc.lineasFacturas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.LineasFactura;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCTemplateDAO;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteResultSetExtractor;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClientesRowMapper;

@Repository
public class LineasFacturaJDBCTemplateDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT =  "SELECT COUNT(*) FROM LINEAS_FACTURA";
	
	private static final String SELECT_ALL =  "SELECT * FROM LINEAS_FACTURA";
	private static final String SELECT_BY_ID =  "SELECT * FROM LINEAS_FACTURA WHERE LINEA = ?";
	
	private static final String INSERT =  "INSERT INTO lineas_factura(linea,factura, articulo, cantidad, importe) VALUES(?,?,?,?,?)";
	private static final String UPDATE =  "UPDATE lineas_factura SET factura=?, articulo=?, cantidad=?, importe=? WHERE linea=?";
	private static final String DELETE =  "DELETE lineas_factura WHERE linea=?";
	
	public int count() {
		return jdbcTemplate.queryForObject(LineasFacturaJDBCTemplateDAO.SELECT_COUNT, Integer.class);
	}
	
	public List<LineasFactura> findAll() {
		
		List<LineasFactura> lineasFactura = this.jdbcTemplate.query(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
						return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
	            
					}
			
				}, 
				new LineasFacturaRowMapper()
		);
		
		return lineasFactura;
	}
	public LineasFactura findById(Integer linea) {
		
		LineasFactura lineasFactura = this.jdbcTemplate.query(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
				PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
	            ps.setInt(1, linea);
	            return ps;
	            
			}
			
		}, new LineasFacturaResultSetExtractor());
		
		return lineasFactura;
	}
	
	public LineasFactura insert(LineasFactura lineasFactura) {
		
		
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
						ps.setInt(1, lineasFactura.getLinea());
						ps.setInt(2,lineasFactura.getIdFactura());
						ps.setInt(3, lineasFactura.getIdArticulo());
						ps.setInt(4,lineasFactura.getCantidad());
						ps.setFloat(5, lineasFactura.getImporte());
						return ps;
					}
					
				});
		if(resultadoOperacion != 1) {
			return null;
		}
		
		return lineasFactura;
	}
	
	public Boolean update(LineasFactura lineasFactura) {
		int resultadoOperacion = this.jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
				
				ps.setInt(1, lineasFactura.getLinea());
				ps.setInt(2,lineasFactura.getIdFactura());
				ps.setInt(3, lineasFactura.getIdArticulo());
				ps.setInt(4,lineasFactura.getCantidad());
				ps.setFloat(5, lineasFactura.getImporte());
				
				return ps;
			}});
		return resultadoOperacion == 1;
	}
	
	public Boolean delete(LineasFactura lineasFactura) {
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {

					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						
						PreparedStatement ps = con.prepareStatement(DELETE, Statement.CLOSE_CURRENT_RESULT);
						ps.setInt(1, lineasFactura.getLinea());
						return ps;
					}
					
				});
		
		
		return resultadoOperacion == 1;
	}

}

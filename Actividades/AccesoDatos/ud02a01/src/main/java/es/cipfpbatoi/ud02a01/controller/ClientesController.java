package es.cipfpbatoi.ud02a01.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCTemplateDAO;


@RestController
@RequestMapping("/clientes")
public class ClientesController {
	
	 
	@Autowired
	ClienteJDBCTemplateDAO clienteJDBCDAOTemplateDAO;
	
	@PostMapping("")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Cliente create(@RequestBody Cliente cliente) {
		
		Cliente clienteInsertado = this.clienteJDBCDAOTemplateDAO.insert(cliente);
		if(clienteInsertado == null) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entitidad no creada");
		}
		
		return clienteInsertado;
	}
	

	@GetMapping("/count")
	public Integer count() {
		return this.clienteJDBCDAOTemplateDAO.count();
	}
	
	@GetMapping("")
	public List<Cliente> findAll() {
		return this.clienteJDBCDAOTemplateDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public Cliente findById(@PathVariable int id) {
		Cliente cliente = this.clienteJDBCDAOTemplateDAO.findById(id);
		if(cliente == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Entidad no encontrada");
		}
		return cliente;
	}
	
	
	@PutMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason = "Entidad actualizada")
	public void update(@PathVariable int id, @RequestBody Cliente cliente) {
		
		boolean update = this.clienteJDBCDAOTemplateDAO.update(cliente);
		if(!update) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Entidad no actualizada");
		}
		
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entidad eliminada")
	
	public void delete(@PathVariable int id, @RequestBody Cliente cliente) {
		
		boolean deleted = this.clienteJDBCDAOTemplateDAO.delete(cliente);
		if(!deleted) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Entidad no eliminada");
		}
	}
}

package es.cipfpbatoi.ud02a01.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import es.cipfpbatoi.ud02a01.model.Grupo;
import es.cipfpbatoi.ud02a01.repository.jdbc.grupos.GrupoJDBCTemplateDAO;

@RestController
@RequestMapping("/grupos")
public class GruposController {
	@Autowired
	GrupoJDBCTemplateDAO grupoJDBCTemplateDAO;
	
	@PostMapping("")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Grupo create(@RequestBody Grupo grupo) {
		
		Grupo grupoInsertado = this.grupoJDBCTemplateDAO.insert(grupo);
		if(grupoInsertado == null) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entidad no creada");
		}
		return grupoInsertado;	
	}
	
	@GetMapping("/count")
	public Integer count() {
		return this.grupoJDBCTemplateDAO.count();
	}
	
	@GetMapping("")
	public List<Grupo> findAll(){
		return this. grupoJDBCTemplateDAO.findAll();
	}

	@GetMapping("/{id}")
	public Grupo findById(@PathVariable int id) {
		Grupo grupo = this.grupoJDBCTemplateDAO.findById(id);
		if(grupo == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Entidad no encontrada");
		}
		return grupo;
		
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason = "Entidad actualizada")
	public void update(@PathVariable int id, @RequestBody Grupo grupo) {
		boolean update = this.grupoJDBCTemplateDAO.update(grupo);
		if(!update) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entidad no actualizada");
		}
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entidad eliminada")
	
	public void delete(@PathVariable int id, @RequestBody Grupo grupo) {
		
		boolean deleted = this.grupoJDBCTemplateDAO.delete(grupo);
		if(!deleted) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entidad no eliminada");
		}
	}
}

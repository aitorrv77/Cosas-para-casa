package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Articulo;

public class ArticulosRowMapper implements RowMapper<Articulo> {

	@Override
	public Articulo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Articulo articulo = new Articulo();
		articulo.setId(rs.getInt("id"));
		articulo.setNombre(rs.getString("nombre"));
		articulo.setPrecio(rs.getDouble("precio"));
		articulo.setCodigo(rs.getString("codigo"));
		articulo.setGrupo(rs.getInt("grupo"));
		return articulo;
	}
}
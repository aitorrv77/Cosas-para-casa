package es.cipfpbatoi.ud02a01.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.annotation.Resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import es.cipfpbatoi.ud02a01.model.Factura;
import es.cipfpbatoi.ud02a01.repository.csv.FacturasCsvDAO;
import es.cipfpbatoi.ud02a01.repository.jdbc.facturas.FacturasJDBCTemplateDAO;

@RestController
@RequestMapping("/facturas")
public class FacturasController {

	 
		@Autowired
		FacturasJDBCTemplateDAO facturaJDBCTemplateDAO;
		
		@Autowired
		FacturasCsvDAO facturasCsvDAO;
	
		@PostMapping("")
		@ResponseStatus(code=HttpStatus.CREATED)
		public Factura create(@RequestBody Factura factura) {
			
			Factura facturaInsertada = this.facturaJDBCTemplateDAO.insert(factura);
			if(facturaInsertada == null) {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Entitidad no creada");
			}
			
			return facturaInsertada;
		}
		

		@GetMapping("/count")
		public Integer count() {
			return this.facturaJDBCTemplateDAO.count();
		}
		
		
		@GetMapping(value="")
		public List<Factura> findAll(@RequestParam(required = false, 
												      defaultValue = "false")
													  Boolean lineas)
									  {
			if(lineas) {
				return this.facturaJDBCTemplateDAO.findWithAll();
				}
			return this.facturaJDBCTemplateDAO.findAll();
		}
		
		@GetMapping(value="/{id}", params = "fileFormat")
		public ResponseEntity<ByteArrayResource> export(@RequestParam String fileFormat, @PathVariable int id){
			
			Factura factura = this.facturaJDBCTemplateDAO.findByIdWithAll(id);
			
			File file = null;
			switch(fileFormat.toLowerCase()) {
			case "csv":
				file = this.facturasCsvDAO.writeCSV(factura);
				break;
				
			default:
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"fileFormat not supported");
			}
			if(file == null) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"File not genereated");
			}
			
			Path path = Paths.get(file.getAbsolutePath());
			ByteArrayResource resource;
			
			try {
				resource = new ByteArrayResource(Files.readAllBytes(path));
				
				return ResponseEntity.ok()
						.header("Content-disposition", "attachment; filename="+file.getName())
						.header("Cache-Control", "no-cache", "no-store", "must-revalidate" )
						.header("Pragma", "no-cache")
						.header("Expires", "0")
						.contentLength(file.length())
						.contentType(MediaType.APPLICATION_OCTET_STREAM)
						.body(resource);
			} catch (IOException e) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"File cannot be downloaded.");
			}
			finally {
				file.delete();
			}
		}
		

		
		
		
		@GetMapping("/{id}")
		public Factura findById(@PathVariable int id) {
			Factura factura = this.facturaJDBCTemplateDAO.findById(id);
			if(factura == null) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Entidad no encontrada");
			}
			return factura;
		}
		
		
		@PutMapping("/{id}")
		@ResponseStatus(code=HttpStatus.NO_CONTENT, reason = "Entidad actualizada")
		public void update(@PathVariable int id, @RequestBody Factura factura) {
			
			boolean update = this.facturaJDBCTemplateDAO.update(factura);
			if(!update) {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Entidad no actualizada");
			}
			
		}
		
		@DeleteMapping("/{id}")
		@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entidad eliminada")
		
		public void delete(@PathVariable int id, @RequestBody Factura factura) {
			
			boolean deleted = this.facturaJDBCTemplateDAO.delete(factura);
			if(!deleted) {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Entidad no eliminada");
			}
		}
}

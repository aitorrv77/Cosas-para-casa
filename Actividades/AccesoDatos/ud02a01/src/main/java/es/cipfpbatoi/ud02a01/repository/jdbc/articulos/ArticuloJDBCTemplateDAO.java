package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Articulo;

@Repository
public class ArticuloJDBCTemplateDAO {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT =  "SELECT COUNT(*) FROM ARTICULOS";
	
	private static final String SELECT_ALL =  "SELECT * FROM ARTICULOS";
	private static final String SELECT_BY_ID =  "SELECT * FROM ARTICULOS WHERE ID = ?";
	
	private static final String INSERT = "INSERT INTO articulos(nombre,precio,codigo,grupo) VALUES(?,?,?,?)";
	private static final String UPDATE = "UPDATE articulos SET nombre=?, precio=?, codigo=?, grupo=? WHERE id=?";
	private static final String DELETE = "DELETE articulos WHERE id=?";
	
	public int count() {
		return jdbcTemplate.queryForObject(ArticuloJDBCTemplateDAO.SELECT_COUNT,Integer.class);
	}
	
	public List<Articulo> findAll(){
		List<Articulo> articulos = this.jdbcTemplate.query(
				new PreparedStatementCreator() {

					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						
						return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
						
					}
					
				}, new ArticulosRowMapper()
				);
		return articulos;
	}
	
	public Articulo findById(Integer id) {
		Articulo articulo = this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{
				PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID,Statement.CLOSE_CURRENT_RESULT);
				ps.setInt(1, id);
				return ps;
			}
		},new ArticuloResultSetExtractor());
		
		return articulo;
	}
	
	public Articulo insert(Articulo articulo) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
				ps.setString(1, articulo.getNombre());
				ps.setDouble(2, articulo.getPrecio());
				ps.setString(3, articulo.getCodigo());
				ps.setInt(4, articulo.getGrupo());
				return ps;
			}
		},keyHolder);
		if(resultadoOperacion != 1) {
			return null;
		}
		articulo.setId(keyHolder.getKey().intValue());
		return articulo;
	}
	
	public Boolean update(Articulo articulo) {
		int resultadoOperacion = this.jdbcTemplate.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(UPDATE,Statement.CLOSE_CURRENT_RESULT);
				
				ps.setString(1, articulo.getNombre());
				ps.setDouble(2, articulo.getPrecio());
				ps.setString(3, articulo.getCodigo());
				ps.setInt(4, articulo.getGrupo());
				ps.setInt(5, articulo.getId());
				return ps;
			}
		});
		return resultadoOperacion == 1;
	}
	
	public Boolean delete(Articulo articulo) {
		
		int resultadoOperacion = this.jdbcTemplate.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(DELETE,Statement.CLOSE_CURRENT_RESULT);
				ps.setInt(1, articulo.getId());
				return ps;
			}
		});
		return resultadoOperacion == 1;
	}

}

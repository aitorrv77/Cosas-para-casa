package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Factura;
import es.cipfpbatoi.ud02a01.model.LineasFactura;
import es.cipfpbatoi.ud02a01.model.Vendedores;

public class FacturasWithAllRowMapper implements RowMapper<Factura>{
	
	private Map<Integer, Factura> facturas = new HashMap<>();

	@Override
	public Factura mapRow(ResultSet rs, int rowNum) throws SQLException {
		Integer idFactura = rs.getInt("facturas.id");
		Factura factura = this.facturas.get(idFactura);
		
		if(factura == null) {
			
			factura = new Factura();
			this.facturas.put(idFactura, factura);
			
			factura.setId(idFactura);
			factura.setFecha(rs.getDate("facturas.fecha"));
			factura.setIdcliente(rs.getInt("clientes.id"));
			factura.setIdvendedor(rs.getInt("vendedores.id"));
			factura.setFormaPago(rs.getString("facturas.formapago"));
			
		}
		
		
		
		Cliente cliente = new Cliente();
		factura.setCliente(cliente);
		
		cliente.setId(rs.getInt("clientes.id"));
		cliente.setNombre(rs.getString("clientes.nombre"));
		cliente.setDireccion(rs.getString("clientes.direccion"));
		
		
		Vendedores vendedor = new Vendedores();
		factura.setVendedor(vendedor);
		
		vendedor.setNombre(rs.getString("vendedores.nombre"));
		
		LineasFactura lineaFactura = new LineasFactura();
		factura.addLineas(lineaFactura);
		
		
		lineaFactura.setLinea(rs.getInt("lineas_factura.linea"));
		lineaFactura.setIdFactura(rs.getInt("lineas_factura.factura"));
		lineaFactura.setIdArticulo(rs.getInt("lineas_factura.articulo"));
		lineaFactura.setCantidad(rs.getInt("lineas_factura.cantidad"));
		lineaFactura.setImporte(rs.getFloat("lineas_factura.importe"));
		
	
	
		return factura;
	}

}

package es.cipfpbatoi.ud02a01.repository.jdbc.lineasFacturas;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Factura;
import es.cipfpbatoi.ud02a01.model.LineasFactura;

public class LineasFacturaRowMapper implements RowMapper<LineasFactura>{

	@Override
	public LineasFactura mapRow(ResultSet rs, int rowNum) throws SQLException {
		
				LineasFactura lineaFactura = new LineasFactura();
				
				lineaFactura.setLinea(rs.getInt("linea"));
				lineaFactura.setIdArticulo(rs.getInt("articulo"));
				lineaFactura.setCantidad(rs.getInt("cantidad"));
				lineaFactura.setImporte(rs.getFloat("importe"));
				
				
				return lineaFactura;
			}

	}




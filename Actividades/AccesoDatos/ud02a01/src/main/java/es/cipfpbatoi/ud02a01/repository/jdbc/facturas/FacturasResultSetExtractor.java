package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import es.cipfpbatoi.ud02a01.model.Factura;

public class FacturasResultSetExtractor implements ResultSetExtractor<Factura>{
	@Override
	public Factura extractData(ResultSet rs) throws SQLException, DataAccessException {	
		
		if(!rs.next()) {
		return null;
		}

	
		Factura factura = new Factura();
		factura.setId(rs.getInt("id"));
		factura.setFecha(rs.getDate("fecha"));
		factura.setIdcliente(rs.getInt("cliente"));
		factura.setIdvendedor(rs.getInt("vendedor"));
		factura.setFormaPago(rs.getString("formapago"));
		return factura;
		
	}
	

}


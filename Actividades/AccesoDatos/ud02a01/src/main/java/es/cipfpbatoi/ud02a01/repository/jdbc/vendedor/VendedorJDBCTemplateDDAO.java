package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Vendedores;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClientesRowMapper;

@Repository
public class VendedorJDBCTemplateDDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT = "SELECT COUNT(*) FROM VENDEDORES";
	private static final String SELECT_ALL = "SELECT * FROM VENDEDORES";
	private static final String SELECT_BY_ID =  "SELECT * FROM VENDEDORES WHERE ID = ?";
	
	private static final String INSERT = "INSERT INTO vendedores(nombre,fecha_ingreso,salario) VALUES(?,?,?)";
	private static final String UPDATE = "UPDATE vendedores SET nombre=?, fecha_ingreso=?, salario =? WHERE id =?";
	private static final String DELETE = "DELETE vendedores WHERE id=?";
	
	
	public int count() {
		return this.jdbcTemplate.queryForObject(VendedorJDBCTemplateDDAO.SELECT_COUNT, Integer.class);
	}
	
	public List<Vendedores> findAll(){ 
		
		List<Vendedores> vendedor = this.jdbcTemplate.query(
			new PreparedStatementCreator(){
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{
				
				return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
			}
			},
			new VendedoresRowMapper()
	);
	return vendedor;
	
	}
	public Vendedores findById(Integer id) {
		Vendedores vendedor = this.jdbcTemplate.query(new PreparedStatementCreator(){
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
			PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
			ps.setInt(1, id);
			return ps;
			}
			
		}, new VendedorResultSetExtractor());
		return vendedor;
	}
	
	public Vendedores insert(Vendedores vendedor) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
					
					@Override
					public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
						PreparedStatement ps = con.prepareStatement(INSERT, Statement.CLOSE_CURRENT_RESULT);
						ps.setString(1, vendedor.getNombre());
						ps.setDate(2, vendedor.getFechaIngreso());
						ps.setDouble(3, vendedor.getSalario());
						return ps;
					}
				},keyHolder);
				
		if(resultadoOperacion != 1) {
				return null;	
				}
		vendedor.setId(keyHolder.getKey().intValue());
		return vendedor;
	}
	
	public Boolean update (Vendedores vendedor) {
		int resultadoOperacion = this.jdbcTemplate.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(UPDATE, Statement.CLOSE_CURRENT_RESULT);
				
				ps.setString(1, vendedor.getNombre());
				ps.setDate(2, vendedor.getFechaIngreso());
				ps.setDouble(3, vendedor.getSalario());
				ps.setInt(4, vendedor.getId());
				return ps;
			}
		});
		return resultadoOperacion ==1;
	}
	
	public Boolean delete(Vendedores vendedor) {
		int resultadoOperacion = this.jdbcTemplate.update(
				new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(DELETE,Statement.CLOSE_CURRENT_RESULT);
				ps.setInt(1, vendedor.getId());
				return ps;
			}
		});

		return resultadoOperacion == 1;
	}
}



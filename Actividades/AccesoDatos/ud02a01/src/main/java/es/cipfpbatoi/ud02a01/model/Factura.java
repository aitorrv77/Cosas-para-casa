package es.cipfpbatoi.ud02a01.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



@JsonInclude(Include.NON_NULL)
public class Factura {
	
	private Integer id;
	private Date fecha;
	private Integer idcliente;
	private Integer idvendedor;
	private String formaPago;
	
	private Vendedores vendedor;
	private Cliente cliente;
	private LineasFactura lineaFactura;
	
	private List<LineasFactura> lFactura = new ArrayList<>();
	
	public void addLineas(LineasFactura lineaFactura) {
		this.lFactura.add(lineaFactura);
	}
	
	public Factura() {
	
	}
	

	public List<LineasFactura> getlFactura() {
		return lFactura;
	}

	public void setlFactura(List<LineasFactura> lFactura) {
		this.lFactura = lFactura;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getIdcliente() {
		return idcliente;
	}

	public void setIdcliente(int idcliente) {
		this.idcliente = idcliente;
	}

	public int getIdvendedor() {
		return idvendedor;
	}

	public void setIdvendedor(int idvendedor) {
		this.idvendedor = idvendedor;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public Vendedores getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedores vendedor) {
		this.vendedor = vendedor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public LineasFactura getLineaFactura() {
		return lineaFactura;
	}

	public void setLineaFactura(LineasFactura lineaFactura) {
		this.lineaFactura = lineaFactura;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Factura other = (Factura) obj;
		return id == other.id;
	}
	
	
	
	
	
}

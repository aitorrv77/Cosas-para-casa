package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Grupo;

public class GrupoRowMapper implements RowMapper<Grupo>{

	@Override
	public Grupo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Grupo grupo = new Grupo();
		grupo.setId(rs.getInt("id"));
		grupo.setDescripcion(rs.getString("descripcion"));
		return grupo;
	}
	
	
}
